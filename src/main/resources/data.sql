insert into cliente (dni, nombre, apellido, fnacimiento)
values
('12345678','Ariel','Martinez','2001-08-16'),
('87654321','Natalia','Correa','1996-10-15'),
('12348765','Mariano','Santo','1992-12-01');

insert into producto (codigo, descripcion, precio, marca, stock)
values
('0010','Teclado',500,'Razer',9),
('0030','Mouse',250,'Generico',10),
('0060','Parlante',500,'JBL',6),
('0090','Monitor',2000,'Viewsonic',2);

insert into venta (dnicliente, preciofinal, fcompra)
values
('12345678', 1250,'2022-08-09'),
('87654321', 2500,'2022-08-08');

insert into linea (idventa, codproducto, cantidad, precio)
values
(1,'0010',1,500),
(1,'0030',3,250),
(2,'0060',3,500),
(2,'0030',2,250),
(2,'0060',1,500);