package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class LineaGet implements Serializable {
    private final int id;
    private final int cantidad;
    private final float precio;
}
