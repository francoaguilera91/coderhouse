package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class VentaProducto implements Serializable {
    private final String codigo;
}
