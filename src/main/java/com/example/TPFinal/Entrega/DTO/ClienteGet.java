package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClienteGet implements Serializable {
    private final String dni;
    private final String nombre;
    private final String apellido;
    private final int edad;
}
