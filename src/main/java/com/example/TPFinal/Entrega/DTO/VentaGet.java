package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class VentaGet implements Serializable {
    private final String dniCliente;
    private final float precioFinal;
    private final LocalDate fechaCompra;
}
