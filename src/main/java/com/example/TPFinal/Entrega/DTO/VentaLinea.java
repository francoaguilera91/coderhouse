package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class VentaLinea implements Serializable {
    private final VentaProducto producto;
    private final int cantidad;
}
