package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductoGet implements Serializable {
    private final String codigo;
    private final String descripcion;
    private final float precio;
    private final String marca;
    private final int stock;
}
