package com.example.TPFinal.Entrega.DTO;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class VentaPost implements Serializable {
    private final int id;
    private final VentaCliente cliente;
    private final List<VentaLinea> lineas;
}
