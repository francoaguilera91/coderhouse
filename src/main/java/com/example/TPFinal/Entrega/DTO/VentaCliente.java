package com.example.TPFinal.Entrega.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class VentaCliente implements Serializable {
    private final String dni;
}
