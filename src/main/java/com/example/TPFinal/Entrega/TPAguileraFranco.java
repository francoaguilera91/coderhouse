package com.example.TPFinal.Entrega;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TPAguileraFranco {

	public static void main(String[] args) {
		SpringApplication.run(TPAguileraFranco.class, args);
	}

}
