package com.example.TPFinal.Entrega.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "Producto")
public class Producto {

    @Id
    @Column(name = "CODIGO")
    String codigo;

    @Column(name = "DESCRIPCION")
    String descripcion;

    @Column(name = "PRECIO")
    float precio;

    @Column(name = "MARCA")
    String marca;

    @Column(name = "STOCK")
    int stock;

    @OneToMany(mappedBy = "producto", cascade = CascadeType.ALL)
    private List<Linea> lineas;

    @Override
    public String toString() {
        return "Producto{" +
                "codigo=" + codigo +
                ", descripción='" + descripcion + '\'' +
                ", precio=" + precio +
                ", marca='" + marca + '\'' +
                ", stock=" + stock +
                '}';
    }
}
