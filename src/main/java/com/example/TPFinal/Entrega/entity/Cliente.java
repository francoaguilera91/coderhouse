package com.example.TPFinal.Entrega.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "CLIENTE")
public class Cliente {

    @Id
    @Column(name="DNI")
    private String dni;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name="APELLIDO")
    private String apellido;

    @Column(name="FNACIMIENTO")
    private Date fNacimiento;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<Venta> venta;

    @Override
    public String toString() {
        return "Cliente{" +
                "dni='" + dni + '\'' +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", fNacimiento=" + fNacimiento +
                '}';
    }
}
