package com.example.TPFinal.Entrega.repository;

import com.example.TPFinal.Entrega.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepository extends JpaRepository<Producto, String> {
}