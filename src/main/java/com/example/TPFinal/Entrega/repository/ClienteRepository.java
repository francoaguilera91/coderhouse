package com.example.TPFinal.Entrega.repository;

import com.example.TPFinal.Entrega.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClienteRepository extends JpaRepository<Cliente, java.lang.String> {



    Optional<Cliente> findById(String dni);

}