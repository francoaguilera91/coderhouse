package com.example.TPFinal.Entrega.repository;

import com.example.TPFinal.Entrega.entity.Cliente;
import com.example.TPFinal.Entrega.entity.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VentaRepository extends JpaRepository<Venta, Integer> {

    List<Venta> findAllByCliente(Cliente cliente);
}