package com.example.TPFinal.Entrega.repository;

import com.example.TPFinal.Entrega.entity.Linea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineaRepository extends JpaRepository<Linea, Integer> {
}