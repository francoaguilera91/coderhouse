package com.example.TPFinal.Entrega.errorHandler;

public class ApiException extends Exception {
    private String message;
    public ApiException(String message) {
        super(message);
    }
}
