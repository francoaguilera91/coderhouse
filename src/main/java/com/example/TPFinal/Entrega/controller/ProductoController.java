package com.example.TPFinal.Entrega.controller;

import com.example.TPFinal.Entrega.DTO.ProductoGet;
import com.example.TPFinal.Entrega.entity.Producto;
import com.example.TPFinal.Entrega.service.ProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/producto")
public class ProductoController {

    private final ProductoService productoService;

    @GetMapping("")
    public List<ProductoGet> getProducto() {
        return productoService.getAll();
    }

    @GetMapping("/{cod}")
    public ProductoGet searchProducto(@PathVariable String cod) {
        return productoService.searchByCod(cod);
    }

    @PostMapping("/crear")
    public ProductoGet crearProducto(@RequestBody Producto producto) {
        return productoService.create(producto);
    }

    @PostMapping("/actualizar")
    public ProductoGet updateProducto(@RequestBody Producto producto) {
        return productoService.update(producto);
    }
}
