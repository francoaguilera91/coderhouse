package com.example.TPFinal.Entrega.controller;

import com.example.TPFinal.Entrega.DTO.LineaGet;
import com.example.TPFinal.Entrega.entity.Linea;
import com.example.TPFinal.Entrega.service.LineaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/linea")
public class LineaController {

    private final LineaService lineaService;

    @GetMapping("")
    public List<LineaGet> getLinea() {
        return lineaService.getAll();
    }

    @GetMapping("/{id}")
    public LineaGet searchLinea(@PathVariable int id) {
        return lineaService.searchByID(id);
    }

    @PostMapping("/crear")
    public LineaGet crearLineaVenta(@RequestBody Linea linea) {
        return lineaService.create(linea);
    }

    @PostMapping("/actualizar")
    public LineaGet updateLinea(@RequestBody Linea linea) {
        return lineaService.update(linea);
    }
}
