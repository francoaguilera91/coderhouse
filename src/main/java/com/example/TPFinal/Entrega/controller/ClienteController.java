package com.example.TPFinal.Entrega.controller;

import com.example.TPFinal.Entrega.entity.Cliente;
import com.example.TPFinal.Entrega.DTO.ClienteGet;
import com.example.TPFinal.Entrega.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping ("/cliente")
public class ClienteController {
    private final ClienteService clienteService;

    @GetMapping("")
    public List<ClienteGet> getCliente() {
        return clienteService.getAll();
    }

    @GetMapping("/{dni}")
    public ClienteGet searchClientes(@PathVariable String dni) {
        return clienteService.searchByDNI(dni);
    }

    @PostMapping("/crear")
    public ClienteGet createCliente(@RequestBody Cliente cliente) {  return clienteService.create(cliente); }

    @PostMapping("/actualizar")
    public ClienteGet updateCliente(@RequestBody Cliente cliente) {return clienteService.update(cliente);   }

}
