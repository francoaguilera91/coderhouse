package com.example.TPFinal.Entrega.controller;

import com.example.TPFinal.Entrega.DTO.VentaPost;
import com.example.TPFinal.Entrega.DTO.VentaGet;
import com.example.TPFinal.Entrega.entity.Venta;
import com.example.TPFinal.Entrega.service.VentaService;
import com.example.TPFinal.Entrega.errorHandler.ApiException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/venta")
public class VentaController {
    private final VentaService ventaService;

    @GetMapping("")
    public List<VentaGet> getVentas() {
        return ventaService.getAll();
    }

    @GetMapping("/{id}")
    public VentaGet searchVentas(@PathVariable int id) {
        return ventaService.searchById(id);
    }

    @GetMapping("/dni/{dni}")
    public List<VentaGet> searchVentasByDNI(@PathVariable String dni) {
        return ventaService.searchByDNI(dni);
    }

    @PostMapping("/crear")
    public VentaGet createVenta(@RequestBody VentaPost ventaPost) throws ApiException {
        return ventaService.create(ventaPost);
    }

    @PostMapping("/actualizar")
    public VentaGet updateVentas(@RequestBody Venta venta) {
        return ventaService.update(venta);
    }


}
