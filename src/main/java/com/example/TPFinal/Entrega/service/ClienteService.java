package com.example.TPFinal.Entrega.service;

import com.example.TPFinal.Entrega.DTO.ClienteGet;
import com.example.TPFinal.Entrega.entity.Cliente;

import java.util.List;


public interface ClienteService {
    List<ClienteGet> getAll();

    ClienteGet searchByDNI(String dni);

    ClienteGet update(Cliente cliente);

    ClienteGet create(Cliente cliente);
}
