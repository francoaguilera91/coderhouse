package com.example.TPFinal.Entrega.service.impl;

import com.example.TPFinal.Entrega.DTO.ProductoGet;
import com.example.TPFinal.Entrega.entity.Producto;
import com.example.TPFinal.Entrega.repository.ProductoRepository;
import com.example.TPFinal.Entrega.service.ProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductoServiceImpl implements ProductoService {

    private final ProductoRepository productoRepository;
    @Override
    public List<ProductoGet> getAll() {
        List<Producto> posts = productoRepository.findAll();
        return posts.stream()
                .map(this::productoToProductoGet)
                .collect(Collectors.toList());
    }
    @Override
    public ProductoGet searchByCod(String cod) { return productoToProductoGet(productoRepository.findById(cod).orElse(null));  }

    @Override
    public ProductoGet update(Producto producto) {
        return productoToProductoGet(productoRepository.save(producto));
    }

    @Override
    public ProductoGet create(Producto producto) {
        return productoToProductoGet(productoRepository.save(producto));
    }

    private ProductoGet productoToProductoGet(Producto producto){
        return new ProductoGet(producto.getCodigo(), producto.getDescripcion(), producto.getPrecio(), producto.getMarca(), producto.getStock());
    }
}
