package com.example.TPFinal.Entrega.service.impl;

import com.example.TPFinal.Entrega.DTO.ClienteGet;
import com.example.TPFinal.Entrega.entity.Cliente;
import com.example.TPFinal.Entrega.repository.ClienteRepository;
import com.example.TPFinal.Entrega.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;

    @Override
    public List<ClienteGet> getAll() {
        List<Cliente> posts = clienteRepository.findAll();
        return posts.stream()
                .map(this::clienteToClienteGet)
                .collect(Collectors.toList());
    }
    @Override
    public ClienteGet searchByDNI(String dni) {
        return clienteToClienteGet(clienteRepository.findById(dni).orElse(null));
    }

    @Override
    public ClienteGet update(Cliente cliente) {
        return clienteToClienteGet(clienteRepository.save(cliente));
    }

    @Override
    public ClienteGet create(Cliente cliente) {
        return clienteToClienteGet(clienteRepository.save(cliente));
    }

    private ClienteGet clienteToClienteGet(Cliente cliente){
        int age = Period.between(cliente.getFNacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                ,LocalDate.now())
                .getYears();
        return new ClienteGet(cliente.getDni(), cliente.getNombre(), cliente.getApellido(), age);
    }
}
