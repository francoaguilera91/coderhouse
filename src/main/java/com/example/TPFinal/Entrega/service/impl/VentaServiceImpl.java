package com.example.TPFinal.Entrega.service.impl;

import com.example.TPFinal.Entrega.DTO.VentaLinea;
import com.example.TPFinal.Entrega.entity.*;
import com.example.TPFinal.Entrega.DTO.VentaGet;
import com.example.TPFinal.Entrega.DTO.VentaPost;
import com.example.TPFinal.Entrega.errorHandler.ApiException;
import com.example.TPFinal.Entrega.repository.ClienteRepository;
import com.example.TPFinal.Entrega.repository.ProductoRepository;
import com.example.TPFinal.Entrega.repository.VentaRepository;
import com.example.TPFinal.Entrega.service.VentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VentaServiceImpl implements VentaService {

    private final VentaRepository ventaRepository;
    private final ClienteRepository clienteRepository;
    private final ProductoRepository productoRepository;

    @Override
    public List<VentaGet> getAll() {
        List<Venta> posts = ventaRepository.findAll();
        return posts.stream()
                .map(this::ventaToVentaGet)
                .collect(Collectors.toList());
    }

    @Override
    public VentaGet searchById(int id) {
        return ventaToVentaGet(ventaRepository.findById(id).orElse(null));
    }

    @Override
    public List<VentaGet> searchByDNI(String dni) {
        Cliente cliente = clienteRepository.findById(dni).orElse(null);
        List<Venta> posts = ventaRepository.findAllByCliente(cliente);
        return posts.stream()
                .map(this::ventaToVentaGet)
                .collect(Collectors.toList());
    }

    @Override
    public VentaGet update(Venta venta) {
        return ventaToVentaGet(ventaRepository.save(venta));
    }

    @Override
    public VentaGet create(VentaPost ventaPost) throws ApiException {

        boolean existCliente = existCliente(ventaPost.getCliente().getDni());
        boolean existProducto = existProductoAndStock(ventaPost.getLineas()) ;

        if (!existCliente) {
            throw new ApiException("No existe el cliente");
        }
        if (!existProducto){
            throw new ApiException("No hay disponibilidad de productos");
        }

        LocalDate fechaCompra = obtenerFechaCompra();
        Cliente cliente = dniToCliente(ventaPost.getCliente().getDni());
        List<Linea> lineas = obtenerLineas(ventaPost.getLineas());
        float precioFinal = calculatePrecioFinal(lineas);

        Venta venta = new Venta(
                cliente,
                precioFinal,
                fechaCompra,
                lineas);

        agregarVentaEnLineas(lineas,venta);

        ventaRepository.save(venta);

        reducirStock(lineas);

        return ventaToVentaGet(venta);
    }

    private void agregarVentaEnLineas(List<Linea> lineas, Venta venta) {
        for (Linea linea : lineas){
            linea.setVenta(venta);
        }
    }

    private void reducirStock(List<Linea> lineas) {
        for (Linea linea : lineas){
            Producto producto = linea.getProducto();
            int stock = producto.getStock();
            int cantidad = linea.getCantidad();
            int newStock = stock - cantidad;

            producto.setStock(newStock);
            productoRepository.save(producto);
        }
    }

    private List<Linea> obtenerLineas(List<VentaLinea> ventaLineas) {
        List<Linea> lineas = new ArrayList<>();

        for (VentaLinea ventaLinea : ventaLineas){
            Producto producto = productoRepository.findById(ventaLinea.getProducto().getCodigo()).orElse(null);
            int cantidad = ventaLinea.getCantidad();
            float precioLinea = calculatePrecioLinea(producto,cantidad);

            Linea linea = new Linea(
                    producto,
                    cantidad,
                    precioLinea);

            lineas.add(linea);
        }
        return lineas;
    }

    private LocalDate obtenerFechaCompra() {
        LocalDate fechaCompra = LocalDate.now();
        RestTemplate restTemplate = new RestTemplate();
        APIClock APIClock = restTemplate.getForObject("http://worldclockapi.com/api/json/utc/now", APIClock.class);
        String fechaCompraString = APIClock.getCurrentDateTime();
        try {
            fechaCompra = new SimpleDateFormat("yyyy-MM-dd'T'mm:ss'Z'").parse(fechaCompraString).toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        } catch (ParseException e) {
        }
        return fechaCompra;
    }

    private boolean existProductoAndStock(List<VentaLinea> lineas) {
        for (VentaLinea ventaLinea : lineas){
            if (!productoRepository.existsById(ventaLinea.getProducto().getCodigo())){
                return false;
            } else {
                Producto producto = productoRepository.findById(ventaLinea.getProducto().getCodigo()).orElse(null);
                int stock = producto.getStock();
                if (stock < ventaLinea.getCantidad()){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean existCliente(String dni) {
        return clienteRepository.existsById(dni);
    }

    private float calculatePrecioFinal(List<Linea> lineas) {
        float precioFinal = 0;
        for (Linea linea : lineas){
            precioFinal = precioFinal + linea.getPrecio() * linea.getCantidad();
        }
        return precioFinal;
    }

    private float calculatePrecioLinea(Producto producto, int cantidad) {
        return cantidad * producto.getPrecio();
    }

    private VentaGet ventaToVentaGet(Venta venta){
        return new VentaGet(venta.getCliente().getDni(), venta.getPrecioFinal(),venta.getFCompra());
    }
    private Cliente dniToCliente(String dni){
        return clienteRepository.findById(dni).orElse(null);
    }

}
