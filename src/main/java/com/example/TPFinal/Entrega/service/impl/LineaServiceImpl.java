package com.example.TPFinal.Entrega.service.impl;

import com.example.TPFinal.Entrega.DTO.LineaGet;
import com.example.TPFinal.Entrega.entity.Linea;
import com.example.TPFinal.Entrega.repository.LineaRepository;
import com.example.TPFinal.Entrega.service.LineaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LineaServiceImpl implements LineaService {

    private final LineaRepository lineaRepository;


    @Override
    public List<LineaGet> getAll() {
        List<Linea> posts = lineaRepository.findAll();
        return posts.stream()
                .map(this::lineaToLineaGet)
                .collect(Collectors.toList());
    }
    @Override
    public LineaGet searchByID(int id) {
        return lineaToLineaGet(lineaRepository.findById(id).orElse(null));
    }

    @Override
    public LineaGet update(Linea linea) {
        return lineaToLineaGet(lineaRepository.save(linea));
    }

    @Override
    public LineaGet create(Linea linea) {
        return lineaToLineaGet(lineaRepository.save(linea));
    }

    private LineaGet lineaToLineaGet(Linea linea){
        return new LineaGet(linea.getId(), linea.getCantidad(), linea.getPrecio());
    }
}
