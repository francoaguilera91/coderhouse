package com.example.TPFinal.Entrega.service;



import com.example.TPFinal.Entrega.DTO.LineaGet;
import com.example.TPFinal.Entrega.entity.Linea;

import java.util.List;

public interface LineaService {
    List<LineaGet> getAll();

    LineaGet searchByID(int id);

    LineaGet update(Linea linea);

    LineaGet create(Linea linea);
}
