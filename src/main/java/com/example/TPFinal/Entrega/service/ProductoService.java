package com.example.TPFinal.Entrega.service;

import com.example.TPFinal.Entrega.DTO.ProductoGet;
import com.example.TPFinal.Entrega.entity.Producto;

import java.util.List;

public interface ProductoService {
    List<ProductoGet> getAll();

    ProductoGet searchByCod(String cod);

    ProductoGet update(Producto producto);

    ProductoGet create(Producto producto);
}
