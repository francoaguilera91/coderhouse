package com.example.TPFinal.Entrega.service;

import com.example.TPFinal.Entrega.entity.Venta;
import com.example.TPFinal.Entrega.DTO.VentaGet;
import com.example.TPFinal.Entrega.DTO.VentaPost;
import com.example.TPFinal.Entrega.errorHandler.ApiException;

import java.util.List;

public interface VentaService {
    List<VentaGet> getAll();
    VentaGet searchById(int id);
    List<VentaGet> searchByDNI(String dni);
    VentaGet update(Venta venta);
    VentaGet create(VentaPost ventaPost) throws ApiException;


}
